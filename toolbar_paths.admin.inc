<?php

/**
 * @file
 * Provides settings configuration for the Toolbar Paths module.
 */

/**
 * Form constructor for the settings.
 */
function toolbar_paths_settings() {
  $form = array();

  $form['toolbar_paths_mode'] = array(
    '#type' => 'radios',
    '#title' => t('Use inclusion or exclusion mode'),
    '#options' => array('exclude', 'include'),
    '#default_value' => variable_get('toolbar_paths_mode', NULL),
    '#description' => t('Choose the way of disabling/enabling Toolbar on selected paths (see below).<br/>Use Exclude to disable toolbar on selected paths. Use Include if you want to load toolbar only on selected paths.'),
  );

  $form['toolbar_paths_paths'] = array(
    '#type' => 'textarea',
    '#title' => t('Select path(s) where you want to process Toolbar. Line break for multiple paths'),
    '#default_value' => variable_get('toolbar_paths_paths', NULL),
    '#description' => t('Set Toolbar Paths process when any of the paths above match the page path. Put each path on a separate line. You can use the "*" character as a wildcard and ~ to exclude one or more paths. Use &#060;front&#062; for the site front page.'),
  );

  return system_settings_form($form);
}