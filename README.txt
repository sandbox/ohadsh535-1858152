The Toolbar Paths module provides a simple settings interface which allows you 
to control the display of the drupal 7 Toolbar, by invoking path condtions.

This module may come to use, while using internal iframes and such.
Also handy to tidy up on unique necessities.


Installation
------------
Follow the standard contributed module installation process:
http://drupal.org/documentation/install/modules-themes/modules-7

* Configure the module at Configuration > User interface > Toolbar Paths.


Requirements
------------
Toolbar module - Drupal 7 Core.


Configuration
-------------
Just go to the Toolbar Paths setting, under config/user-interface.
- Choose the mode of Toolbar Paths: include/exclude the toolbar.
- Insert the paths which should be invoked accordingly. (You may use regex)


Support
-------
Please use the issue queue for filing bugs with this module at
http://drupal.org/project/issues/toolbar_paths
